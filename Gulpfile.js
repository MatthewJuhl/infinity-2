// TODO: look into (browser sync)[http://www.browsersync.io/docs/gulp/] instead of livereload

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var minifyCss = require('gulp-minify-css');
var livereload = require('gulp-livereload');
var notify = require('gulp-notify');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
// var changed = require('gulp-changed');
// var imagemin = require('gulp-imagemin');


// Gulp Sass Task
gulp.task('sass', function () {
	console.log('Running sass task...');
	gulp.src('./lib/cms/scss/**/*.{scss,sass}')
		.pipe(notify({message: 'running sass job...'})) // send notification
		.pipe(sourcemaps.init()) // initializes sourcemaps
		.pipe(sass({ // build css
			errLogToConsole: false,
			onError: function (err) { // send osx notifications on errors
				// err has [ 'status', 'file', 'line', 'column', 'message', 'code' ]
				// log the full error because it doesn't all fit in the notification
				console.log('SCSS error in ' + err.file + ' on line ' + err.line + ': ' + err.message);
				return notify({
					title: 'SCSS error on line ' + err.line,
					message: err.message,
					sound: true,
				}).write(err);
			}
		}))
		.pipe(autoprefixer()) // autoprefix
		.pipe(minifyCss({keepBreaks:false})) // minify before writing source maps
		.pipe(sourcemaps.write('./', { debug: true }))  // write source maps into the css file
		.pipe(gulp.dest('./lib/cms/static/')) // write the css file
		.pipe(livereload()) // auto reload browser
});

// minify new images
// gulp.task('images', function () {
// 	var dest = './lib/cms/static';

// 	gulp.src('./lib/cms/images/**/*')
// 		.pipe(changed(dest))
// 		.pipe(imagemin())
// 		.pipe(gulp.dest(dest));
// });

// JS concat, strip debugging and minify
gulp.task('js', function () {
	gulp.src('./lib/cms/js/**/*.js')
		.pipe(notify({message: 'running js task...'})) // send notification
		.pipe(sourcemaps.init())
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(uglify())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./lib/cms/static/'))
		.pipe(livereload()) // auto reload browser
});

// Gulp Reload Task
gulp.task('phtml', function () {
	console.log('Running phtml task...');
	gulp.src('./lib/cms/**/*.phtml')
		.pipe(notify({message: 'running phtml job...'})) // send notification
		.pipe(livereload()) // auto reload browser
});


// Watch scss folder for changes
gulp.task('watch', function () {
	livereload.listen(8912); // 35729 is the default
	// Watches the scss folder for all .scss and .sass files
	// If any file changes, run the sass task
	gulp.watch('./lib/cms/scss/**/*.{scss,sass}', ['sass']);
	gulp.watch('./lib/cms/js/**/*.js', ['js']);
	gulp.watch('./lib/cms/**/*.{php,phtml}', ['phtml']);
});

gulp.task('default', ['sass', 'js']);
