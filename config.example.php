<?
# This is Infinity's default configuration file. Make modifications below and
# save it as `config.php`.

# you'll want to change this in production:
error_reporting(E_ALL);
ini_set('display_errors', 1);

# start by including Infinity's bootstrap file.
require_once 'infinity/bootstrap.php';

# initialize infinity
$infinity = new \Infinity\Infinity;

# grab the path to your website's directory
$local_path = dirname(__FILE__);

# set your configuration options here:
$infinity->setConfig([
    # easy stuff
    'site_name' => 'My Infinitely Awesome Website',
    'admin_email' => 'desk@matthewjuhl.com',

    # This simply tells Infinity where we are. You don't need to change this.
    'root_directory' => $local_path,

    # This tells Infinity where the code for your site lives. You don't need to change this.
    'application_directory' => $local_path . DIRECTORY_SEPARATOR . 'application',

    # Set the location of your data directory. This is where Infinity will store
    # your content database, encryption keys, and so forth. Optimally this
    # directory would be located outside of your web server root somwhere not
    # publicly accessible. You should change this (but you don't have to).
    'data_directory' => $local_path . DIRECTORY_SEPARATOR . 'data',


    # If you want to turn on Infinity's internal logging for debug purposes, set
    # a file name here to write to. Debug messages will get written to this file
    # during each request. If you don't need this functionality, comment or delete
    # this line:
    'infinity_log_file' => $local_path . DIRECTORY_SEPARATOR . 'data/infinity.log'
]);

$infinity->view->setLayout('main-layout');

# tell Infinity you're done with configuration to begin handling the request
$infinity->launch();
