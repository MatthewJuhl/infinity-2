!function (w, d, undefined) {
	'use strict';

	var array_proto = [];
	var forEach = array_proto.forEach;

	// clear all alerts after a threshold

	var alert_display_time = 5500;
	var alerts = d.querySelectorAll('.alert');
	[].forEach.call(alerts, function (alert) {
		setTimeout(function () {
			console.log('showing');
			alert.classList.add('is-init');
		}, 500);
		setTimeout(remove_alert, alert_display_time);
		alert.addEventListener('click', remove_alert);
		function remove_alert () {
			console.log('hiding');
			alert.classList.add('alert--hidden');
			setTimeout(function () {
				alert.remove();
			}, 500);
		}
	});


}(this, document);
