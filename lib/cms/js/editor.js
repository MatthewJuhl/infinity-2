!function (w, d, undefined) {

	var array_proto = [];
	var form = document.getElementById('document_editor');

	var editors = document.querySelectorAll('.editor-textarea');
	array_proto.forEach.call(editors, function (editor) {


		// enable markdown editor

		var hidden_mirror = d.createElement('input');
		hidden_mirror.type = 'hidden';
		hidden_mirror.name = editor.name;
		hidden_mirror.value = editor.value;
		form.appendChild(hidden_mirror);

		editor = bililiteRange.fancyText(editor, Prism.highlightElement);

		var api = bililiteRange(editor);

		api.listen('input', get_change_listener(editor, hidden_mirror));


		bililiteRange(editor).undo(0).data().autoindent = true;
		editor.addEventListener('keydown', function(evt){
			if ((evt.ctrlKey||evt.metaKey) && evt.which == 90) bililiteRange.undo(evt);
			if ((evt.ctrlKey||evt.metaKey) && evt.which == 89) bililiteRange.redo(evt);

			// TODO: add support for ctrl+B and ctrl+I for bold and italic
		});

		bililiteRange(editor).data().autoindent = true;



		// enable auto height adjustments


	});


	function get_change_listener (editor, mirror) {
		return debounce(function () {
			mirror.value = editor.innerText;
		}, 250);
	}


	function debounce (func, threshold){
		if (!threshold) return func;
		var timeout;
		return function(){
			var self = this, args = arguments;
			clearTimeout(timeout);
			timeout = setTimeout(function(){
				func.apply(self, args);
			}, threshold);
		};
	}


}(this, document);
