<?
// @var Infinity $this

# override config options for CMS requests
$this->setConfig([
	'website_directory' => $this->getCMSApplicationDIR()
]);



# ignore ^cms in the URL from this point forward, to simplify routing
$this->request_url = preg_replace('/^cms\//', '', $this->request_url);
# regenerate segments array
$this->getURLSegments(TRUE);
# set default CMS layout
$this->view->setLayout('cms-main-layout');
