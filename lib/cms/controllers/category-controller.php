<?
namespace Infinity;
use \Infinity\Entity\Category;
use \Infinity\Entity\Document;

get('category/list', function ($infinity, $params)
{
	$categories = Category::get_category_tree();
	$this->view->set('categories', $categories);
});

get('category/new', function ($infinity, $params)
{
	$this->render('category/category-form');
});

# save a new category
post('category/new', function ($infinity, $params)
{
	$missing = $this->get_missing_fields(['name', 'template_id']);
	if (count($missing))
	{
		$this->add_flash('You must enter a value for all required fields.', 'error');
		$this->save_form();
		$this->redirect('/cms/category/new');
	}

	$post = $this->get_post();
	$category = new Category;
	$category->name = $post['name'];
	$category->description = $post['description'];
	$category->parent_category_id = $post['parent_category_id'];
	$category->template_id = $post['template_id'];
	if ($category->save())
	{
		$this->add_flash($category->name . ' was created!', 'success');
		$this->redirect('/cms/category/' . $category->id);
	}
	else
	{
		$this->add_flash('An error occurred.', 'error');
		$this->save_form();
		$this->redirect('/cms/category/new');
	}
});

get('category/{{id}}', function ($infinity, $params)
{
	$category = Category::get($params['id']);
	// TODO: 404?
	$subs = Category::where(['parent_category_id' => $category->id]);
	$this->view->set('category', $category);
	$this->view->set('form', [
		'name' => $category->name,
		'description' => $category->description,
		'parent_category_id' => $category->parent_category_id,
		'template_id' => $category->template_id
	]);
	$this->view->set(['subs' => $subs]);
	$documents = Document::where(['category_id' => $category->id]);
	$this->view->set('documents', $documents);
	$this->render('category/category-form');
});

# update category
post('category/{{id}}', function ($infinity, $params)
{

	$missing = $this->get_missing_fields(['name', 'template_id']);
	if (count($missing))
	{
		$this->add_flash('You must enter a value for all required fields.', 'error');
		$this->save_form();
		$this->redirect('/cms/category/' . $params['id']);
	}

	$post = $this->get_post();
	$category = Category::get($params['id']);
	// TODO: if ! 404
	$category->name = $post['name'];
	$category->description = $post['description'];
	$category->parent_category_id = $post['parent_category_id'];
	$category->template_id = $post['template_id'];
	if ($category->save())
	{
		$this->add_flash($category->name . ' was updated!', 'success');
		$this->redirect('/cms/category/' . $category->id);
	}
	else
	{
		$this->add_flash('An error occurred.', 'error');
		$this->save_form();
		$this->redirect('/cms/category/' . $category_id);
	}
});

get('category/{{id}}/new-document', function ($infinity, $params)
{
	$category = Category::get($params['id']);
	// TODO: 404 if !
	$this->view->set('form', [
		'category_id' => $params['id'],
		'template_id' => $category->template_id
	]);
	$this->view->set('category', $category);
	$this->render('document/document-form');
});

post('category/{{id}}/new-document', function ($infinity, $params)
{
	$category = Category::get($params['id']);
	// TODO: 404 if !$category
	$missing = $this->get_missing_fields(['name', 'category_id']);
	if (count($missing))
	{
		$this->add_flash('You must enter a value for all required fields.', 'error');
		$this->save_form();
		$this->redirect('/cms/category/' . $params['id'] . '/new-document');
	}

	$post = $this->get_post();
	$document = new Document;
	$document->name = $post['name'];
	$document->description = $post['description'];
	$document->category_id = $post['category_id'];
	$document->template_id = $post['template_id'];
	if ($document->save())
	{
		$this->add_flash($document->name . ' was created!', 'success');
		$this->redirect('/cms/document/' . $document->id);
	}
	else
	{
		$this->add_flash('An error occurred.', 'error');
		$this->save_form();
		$this->redirect('/cms/category/new');
	}

});

# add a sub category
get('category/{{id}}/new-sub', function ($infinity, $params)
{
	$this->view->set('form', [
		'parent_category_id' => $params['id']
	]);
	$this->render('category/category-form');
});

# add a sub category
post('category/{{id}}/new-sub', function ($infinity, $params)
{
	// TODO: this doesn't really work
	$this->forward('category/new');
});
