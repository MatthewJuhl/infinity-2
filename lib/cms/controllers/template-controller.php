<?
namespace Infinity;
use \Infinity\Entity\Template;
use \Infinity\Entity\TemplateField;

get('template/list', function ($infinity, $params)
{
	$templates = Template::all();
	$this->view->set('templates', $templates);
});

# create a new template
get('template/new', function ($infinity, $params)
{
	$this->view->set('form', $this->get_form());
	$this->render('template/template-form');
});

# save a new template
post('template/new', function ($infinity, $params)
{
	$missing = $this->get_missing_fields(['name']);
	if (count($missing))
	{
		$this->add_flash('You must enter a value for all required fields.', 'error');
		$this->save_form();
		$this->redirect('/cms/template/new');
	}

	$post = $this->get_post();
	$template = new Template;
	$template->name = $post['name'];
	$template->description = $post['description'];
	if ($template->save())
	{
		$this->redirect('/cms/template/' . $template->id);
	}
	else
	{
		$this->add_flash('An error occurred.', 'error');
		$this->save_form();
		$this->redirect('/cms/template/new');
	}
});

# edit a template
get('template/{{id}}', function ($infinity, $params)
{
	$template = Template::get($params['id']);
	$this->view->set('form', [
		'name' => $template->name,
		'description' => $template->description
	]);
	$this->view->set('template', $template);

	$fields = TemplateField::where(['template_id' => $template->id]);
	$this->view->set('fields', $fields);
	$this->render('template/template-form');
});

# save changes to a template
post('template/{{id}}', function ($infinity, $params)
{
	$missing = $this->get_missing_fields(['name']);
	if (count($missing))
	{
		$this->add_flash('You must enter a value for all required fields.', 'error');
		$this->save_form();
		$this->redirect('/cms/template/' . $params['id']);
	}

	$post = $this->get_post();
	$template = Template::get($params['id']);
	if (!$template)
	{
		// TODO: 404
		die('template not found');
	}

	$template->name = $post['name'];
	$template->description = $post['description'];
	if ($template->save())
	{
		$this->redirect('/cms/template/' . $template->id);
	}
	else
	{
		$this->add_flash('An error occurred.', 'error');
		$this->save_form();
		$this->redirect('/cms/template/' . $params['id']);
	}
});

# create a new field
get('template/{{template_id}}/add-field', function ($infinity, $params)
{
	$this->view->set('form', $this->get_form());
	$this->render('template/template-field-form');
});

# save a new field
post('template/{{template_id}}/add-field', function ($infinity, $params)
{
	$missing = $this->get_missing_fields(['name', 'format']);
	if (count($missing))
	{
		$this->add_flash('You must enter a value for all required fields.', 'error');
		$this->save_form();
		$this->redirect('/cms/template/' . $params['template_id'] . '/add-field');
	}

	$post = $this->get_post();
	$template = Template::get($params['template_id']);
	// TODO: if !$template, 404?

	$template_field = new TemplateField;
	$template_field->name = $post['name'];
	$template_field->description = $post['description'];
	$template_field->template_id = $template->id;
	$template_field->format = $post['format'];
	$template_field->sort_order = $post['sort_order'];
	$template_field->default_value = $post['default_value'];
	if ($template_field->save())
	{
		$this->redirect('/cms/template/' . $template->id);
	}
	else
	{
		$this->add_flash('An error occurred.', 'error');
		$this->save_form();
		$this->redirect('/cms/template/' . $params['id'] . '/add-field');
	}
});

# edit a template field
get('template/field/{{field_id}}', function ($infinity, $params)
{
	$template_field = TemplateField::get($params['field_id']);
	// TODO: if !, 404


	$template = Template::get($template_field->template_id);
	$this->view->set('template', $template);
	$this->view->set('form', [
		'name' => $template_field->name,
		'description' => $template_field->description,
		'format' => $template_field->format,
		'sort_order' => $template_field->sort_order,
		'default_value' => $template_field->default_value
	]);

	$this->render('template/template-field-form');
});

# save a template field
post('template/field/{{field_id}}', function ($infinity, $params)
{
	$template_field = TemplateField::get($params['field_id']);
	$missing = $this->get_missing_fields(['name', 'format']);
	if (count($missing))
	{
		$this->add_flash('You must enter a value for all required fields.', 'error');
		$this->save_form();
		$this->redirect('/cms/template/field/' . $params['field_id']);
	}

	$post = $this->get_post();

	$template_field->name = $post['name'];
	$template_field->description = $post['description'];
	$template_field->format = $post['format'];
	$template_field->sort_order = $post['sort_order'];
	$template_field->default_value = $post['default_value'];
	if ($template_field->save())
	{
		$this->redirect('/cms/template/' . $template_field->template_id);
	}
	else
	{
		$this->add_flash('An error occurred.', 'error');
		$this->save_form();
		$this->redirect('/cms/template/field/' . $params['field_id']);
	}
});
