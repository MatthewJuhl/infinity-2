<?
namespace Infinity\Entity;

# edit a field
get('field/{{id}}', function ($infinity, $params)
{
    $field = Field::get($params['id']);
    // TODO: handle field not found

    $form = [
        'name' => $field->name,
        'document_id' => $field->document_id,
        'description' => $field->description,
        'format' => $field->format,
        'template_field_id' => $field->template_field_id
    ];

    $saved_post = $this->get_form();
    $form['field_id'] = $params['id'];

    $this->view->set([
        'field' => $field,
        'form' => array_merge($form, $saved_post)
    ]);

    $this->render('field/field-form');
});


post('field/{{id}}', function ($infinity, $params)
{
    // $this->require_token();
    $missing = $this->get_missing_fields(['name', 'format', 'document_id']);
    if (count($missing))
    {
        $this->add_flash('You must enter a value for all required fields.', 'error');
        $this->save_form();
        $this->redirect('/cms/document/' . $params['id'] . '/new-field');
    }

    $field = Field::get($params['id']);
    // TODO: handle field not found
    $post = $this->get_post();
    // TODO: allow changing document_id? probably not?
    // $field->document_id = $document->id;
    $field->name = $post['name'];
    $field->format = $post['format'];
    $field->description = $post['description'];

    $field->save();
    $this->add_flash("$field->name field updated!", 'success');
    $this->redirect('/cms/document/' . $field->document_id);
});
