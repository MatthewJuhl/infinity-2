<?
namespace Infinity\Entity;
use \Infinity\DB;

# /cms/document/{{id}}/editor
get('document/{{id}}/editor', function ($infinity, $params)
{
	$document = Document::get($params['id']);
	// TODO: handle document not found
	# check for a draft
	$draft = Draft::where(['document_id' => $document->id]);
	$category = Category::get($document->category_id);
	$fields = $document->get_fields();
	$fields = Field::associate_by_template_field_id($fields);

	if (count($draft))
	{
		# if there's a draft, modify all the fields to use the draft values
		$draft = $draft[0];
		$draft_fields = $draft->get_fields();
		$fields_associative = Field::make_associative($fields);


		foreach ($draft_fields as $draft_field)
		{
			$field = $fields_associative[$draft_field->field_id];
			// TODO: watch out for NULLs here, should never happen though
			$field->value = $draft_field->value;
		}

	}
	else
	{
		$draft = NULL;
	}
	$template_fields = TemplateField::where(['template_id' => $document->template_id]);
	TemplateField::sort($template_fields);

	$this->view->set([
		'document' => $document,
		'draft' => $draft,
		'fields' => $fields,
		'template_fields' => $template_fields,
		'category' => $category,
		'id' => $params['id']
	]);

	$this->render('document/editor');
});

post('document/{{id}}/editor', function ($infinity, $params)
{
	$post = $this->get_post();
	$missing = $this->get_missing_fields(['save_action']);
	if (count($missing))
	{
		$this->add_flash('Sorry, an error occurred.', 'error');
		$this->save_form();
		$this->redirect('/cms/document/' . $params['id'] . '/editor');
	}

	$this->params['document'] = Document::get($params['id']);

	if ($post['save_action'] === 'Publish')
	{
		$this->forward('document/' . $params['id'] . '/editor/publish');
	}
	else
	{
		$this->forward('document/' . $params['id'] . '/editor/save-draft');
	}
});

get('document/{{id}}/editor/discard-draft', function ($infinity, $params)
{
	$document = Document::get($params['id']);
	// TODO: handle 404
	$draft = Draft::where(['document_id' => $document->id])[0];
	$draft->delete();
	$this->add_flash('Draft has been deleted.', 'success');
	$this->redirect('/cms/document/' . $params['id'] . '/editor');
});

post('document/{{id}}/editor/save-draft', function ($infinity, $params)
{
	$document = $this->params['document'];
	// TODO: handle 404
	# check for existing draft
	$post = $this->get_post();
	DB::beginTransaction();
	$draft = Draft::where(['document_id' => $document->id]);
	if (!count($draft))
	{
		# save as a new draft
		$draft = new Draft;
		$draft->document_id = $document->id;
	}
	else
	{
		$draft = $draft[0];
	}
	# save regardless, so the updated time gets bumped
	$draft->save();

	foreach ($post['fields'] AS $id => $value)
	{
		$field = FieldDraft::get_by_field_id($id);
		$document_field = Field::get($id);
		if (!$field)
		{
			$this->log('no field!');
			continue;
		}

		$field->value = Field::interpret_posted_value($value, $document_field->format);
		$field->save();
	}
	try
	{
		DB::commit();
		$this->add_flash('Draft has been saved. Click publish when you&rsquo;re ready to go live with it!', 'success');

	}
	catch (Exception $e)
	{
		$this->log('Error while saving draft: ' . $e->getMessage());
		$this->add_flash('Sorry, an error occurred.', 'error');
	}
	finally
	{
		$this->redirect('/cms/document/' . $params['id'] . '/editor');
	}
});


post('document/{{id}}/editor/publish', function ($infinity, $params)
{
	$document = $params['document'];
	$post = $this->get_post();
	DB::beginTransaction();

	# delete the draft
	$draft = Draft::where(['document_id' => $document->id]);
	if (count($draft))
	{
		$draft[0]->delete();
	}

	if (!$document->published)
	{
		$document->published = time();
	}
	$document->save();

	foreach ($post['fields'] AS $id => $value)
	{
		$field = Field::get($id);
		if (!$field)
		{
			$this->log('no field!');
			continue;
		}
		$field->value = Field::interpret_posted_value($value, $field->format);
		$field->save();
	}
	DB::commit();
	$this->add_flash('Changes have been published!', 'success');
	$this->redirect('/cms/document/' . $params['id'] . '/editor');
});





get('category/{{category_id}}', function ($infinity, $params)
{
	$category_id = $params['category_id'];
	$category = Category::get($category_id);
	$child_categories = $category->get_children();
	$documents = Document::where(['category_id' => $category_id]);
});

# /cms/document/{{id}}
get('document/{{id}}', function ($infinity, $params)
{
	$document = Document::get($params['id']);
	// TODO: handle document not found
	$category = Category::get($document->category_id);
	$fields = $document->get_fields();

	$this->view->set([
		'document' => $document,
		'form' => [
			'name' => $document->name,
			'description' => $document->description,
			'category_id' => $document->category_id,
			'template_id' => $document->template_id
		],
		'fields' => $fields,
		'category' => $category,
		'id' => $params['id']
	]);

	$this->render('document/document-form');
});

# /cms/document/{{id}}
post('document/{{id}}', function ($infinity, $params)
{
	// $this->require_token();
	$missing = $this->get_missing_fields(['name']);
	if (count($missing))
	{
		$this->add_flash('You must enter a value for all required fields.', 'error');
		$this->save_form();
		$this->redirect('/cms/document/' . $params['id']);
	}

	$document = Document::get($params['id']);
	// TODO: handle document not found
	$post = $this->get_post();

	DB::beginTransaction();
	$document->name = $post['name'];
	$document->description = $post['description'];
	$document->category_id = $post['category_id'];
	$document->template_id = $post['template_id'];
	$document->save();
	if (@$post['fields'])
	{
		foreach ($post['fields'] AS $id => $value)
		{
			$field = Field::get($id);
			if (!$field)
			{
				$this->log('no field!');
				continue;
			}
			$field->value = Field::interpret_posted_value($value, $field->format);
			$field->save();
		}
	}
	DB::commit();
	$this->add_flash('Document saved!', 'success');
	$this->redirect('/cms/document/' . $params['id']);
});


# add a field
get('document/{{id}}/new-field', function ($infinity, $params)
{
	$document = Document::get($params['id']);
	// TODO: handle document not found

	$form = $this->get_form();
	$form['document_id'] = $params['id'];

	$this->view->set([
		'document' => $document,
		'form' => $form
	]);

	$this->render('field/field-form');
});


post('document/{{id}}/new-field', function ($infinity, $params)
{
	// $this->require_token();
	$missing = $this->get_missing_fields(['name', 'format']);
	if (count($missing))
	{
		$this->add_flash('You must enter a value for all required fields.', 'error');
		$this->save_form();
		$this->redirect('/cms/document/' . $params['id'] . '/new-field');
	}

	$document = Document::get($params['id']);
	// TODO: handle document not found
	$post = $this->get_post();

	$field = new Field($post);
	$field->document_id = $document->id;
	$field->save();
	$this->add_flash("$field->name field added!", 'success');
	$this->redirect('/cms/document/' . $params['id']);
});

// get('add-test-data', function ()
// {
// 	$document = new Document;
// 	$document->name = 'about';
// 	$document->category_id = 1;
// 	$document->save();

// 	echo '<pre>';
// 	print_r($document);
// 	echo '</pre>';


// 	$field1 = new Field;
// 	$field1->document_id = $document->id;
// 	$field1->name = 'Test Document 1 Field 1';
// 	$field1->value = 'foo bar';
// 	$field1->save();

// 	echo '<pre>';
// 	print_r($field1);
// 	echo '</pre>';


// 	$field2 = new Field;
// 	$field2->document_id = $document->id;
// 	$field2->name = 'Test Document 1 Field 2';
// 	$field2->value = 'bar baz';
// 	$field2->save();
// 	echo '<pre>';
// 	print_r($field2);
// 	echo '</pre>';

// });
