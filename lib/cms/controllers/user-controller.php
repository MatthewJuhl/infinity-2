<?
namespace Infinity;
use Infinity\Entity\User;
// @var Infinity $this

get('user/sign-in', function ($infinity, $params)
{

});

post('user/sign-in', function ($infinity, $params)
{
	$email_address = $_POST['email_address'];
	$password = $_POST['password'];
	try
	{
		if (Auth::sign_in($email_address, $password))
		{
			$user = Auth::get_signed_in_user();
			$this->add_flash("Hello $user->name!", 'success');
			$this->redirect('/cms');
		}
		else
		{
			$this->add_flash('Incorrect email/password.', 'danger');
			$this->redirect('/cms/sign-in');
		}
	}
	catch (Auth_Exception $e)
	{
		$this->add_flash('Please type your email address and password.', 'danger');
		$this->redirect('/cms/sign-in');
	}

});


get('user/sign-out', function ($infinity, $params)
{
   Auth::sign_out();
   $this->add_flash('You have been signed out.');
   $this->redirect('/cms/sign-in');
});


get('user/list', function ($infinity, $params)
{
	$users = User::all();
	$this->view->set([
		'users' => $users
	]);

});

get('user/{{id}}', function ($infinity, $params)
{
	$user = User::get($params['id']);
	$signed_in_user = Auth::get_signed_in_user();

	$is_own_user = $user->id === $signed_in_user->id;

	// TODO: 404 if !
	$this->view->set([
		'user' => $user,
		'form' => [
			'name' => $user->name,
			'email_address' => $user->email_address
		],
		'is_own_user' => $is_own_user
	]);
	$this->render('user/user-form');
});


post('user/{{id}}', function ($infinity, $params)
{

	$missing = $this->get_missing_fields(['name', 'email_address']);
	if (count($missing))
	{
		$this->add_flash('You must enter a value for all required fields.', 'error');
		$this->save_form();
		$this->redirect('/cms/user/new');
	}

	$post = $this->get_post();

	$user = User::get($params['id']);
	// TODO: 404 if !
	$user->name = $post['name'];
	$user->email_address = $post['email_address'];

	if ($user->save())
	{
		$this->add_flash($user->name . ' was updated!', 'success');
		$this->redirect('/cms/user/' . $user->id);
	}
	else
	{
		$this->add_flash('An error occurred.', 'error');
		$this->save_form();
		$this->redirect('/cms/user/' . $user->id);
	}
});
