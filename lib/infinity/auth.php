<?
namespace Infinity;
use Infinity\Entity\User;
class Auth
{

	private static $current_user = NULL;
	private static $encryption_key;

	const SIGNED_IN_USER = 'SIGNED_IN_USER';
	const ENCRYPTION_KEY_FILENAME = 'ek';


	public static function get_encryption_key ()
	{
		if (!self::$encryption_key)
		{
			$data_dir = Infinity::getInstance()->getConfig('data_directory') . DIRECTORY_SEPARATOR;

			if (!file_exists($data_dir . self::ENCRYPTION_KEY_FILENAME))
			{
				self::create_encryption_key_file();
			}
			else
			{
				self::$encryption_key = file_get_contents($data_dir . self::ENCRYPTION_KEY_FILENAME);
			}
		}
		return self::$encryption_key;
	}

	private static function create_encryption_key_file ()
	{
		self::$encryption_key = Security::generate_encryption_key();
		file_put_contents(Infinity::getInstance()->getConfig('data_directory') . DIRECTORY_SEPARATOR . self::ENCRYPTION_KEY_FILENAME, self::$encryption_key);
	}


	static function sign_in ($email = '', $password = '')
	{
		if (!$email || !$password)
		{
			throw new Auth_Exception('Missing email or password.');
		}

		try
		{
			$user = User::get_by_email($email);

			if ($user instanceof User && Security::is_valid_password($password, $user->password))
			{
				self::$current_user = $user;
				$key = self::get_encryption_key();
				@session_start();

				$_SESSION[self::SIGNED_IN_USER] = Security::encrypt(serialize($user), $key);
				Infinity::getInstance()->log('User has been authenticated.');
				return $user;
			}
		}
		catch (Exception $e)
		{
			throw new Auth_Exception($e->getMessage());
		}
		// TODO: sleep to mimic crypt time
		// sleep(2);
		return FALSE;
	}

	static function check_user_password ($password = '')
	{
		$user = self::get_signed_in_user();
		if (!$user instanceof User)
		{
			throw new Auth_Exception('Not signed in');
		}

		return Security::is_valid_password($password, $user->password);
	}

	static function get_signed_in_user ()
	{
		if (self::$current_user instanceof User)
		{
			return self::$current_user;
		}

		try
		{
			@session_start();
			$user = @$_SESSION[self::SIGNED_IN_USER];
			if ($user)
			{
				$key = self::get_encryption_key();
				$user = unserialize(Security::decrypt($user, $key));

				if ($user instanceof User)
				{
					self::$current_user = $user;
					return $user;
				}
			}
		}
		catch (Exception $e) {}

		return FALSE;
	}

	// reloads the stored user object
	static function refresh_user ()
	{
		$id = self::get_signed_in_user_id();
		$user = User::get($id);
		if ($user instanceof User)
		{
			self::$current_user = $user;
			$key = self::get_encryption_key();

			@session_start();
			$_SESSION[self::SIGNED_IN_USER] = Security::encrypt(serialize($user), $key);
		}
	}

	static function get_signed_in_user_id ()
	{
		$user = self::get_signed_in_user();
		if ($user instanceof User)
		{
			return $user->id;
		}

		return FALSE;
	}


	static function is_signed_in ()
	{
		try
		{
			$user = self::get_signed_in_user();

			if ($user)
			{
				return TRUE;
			}
		}
		catch (Exception $e)
		{
			throw Auth_Exception($e->getMessage());
		}

		return FALSE;
	}

	static function sign_out ()
	{
		@session_start();
		$_SESSION[self::SIGNED_IN_USER] = self::$current_user = NULL;
		session_destroy();
		return TRUE;
	}

}

class Auth_Exception extends \Exception {}
