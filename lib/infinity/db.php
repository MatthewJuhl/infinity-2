<?
namespace Infinity;
class DB
{
    private static $data_directory;
    private static $db_filename;
    private static $connection;
    private static $in_transaction = 0;
    private static $logger;

    public static function init ($infinity)
    {

        static::$data_directory = $infinity->getConfig('data_directory');
        static::$db_filename = $infinity->getConfig('db_filename');
        static::$logger = $infinity->logger;
    }

    public static function get_db_filename ()
    {
        if (!static::$data_directory)
        {
            $infinity = Infinity::getInstance();
            static::$data_directory = $infinity->getConfig('data_directory');
            static::$db_filename = $infinity->getConfig('db_filename');
            static::$logger = $infinity->logger;
        }
        return static::$data_directory . DIRECTORY_SEPARATOR . static::$db_filename;
    }

    public static function connect ()
    {

        if (self::$connection) {
            return TRUE;
        }

        $infinity = Infinity::getInstance();

        try
        {
            $infinity->log('connecting to ' . 'sqlite:' . self::get_db_filename() . '...');
            self::$connection = new \PDO('sqlite:' . self::get_db_filename());
            $infinity->log('connected!');
        }
        catch (\PDOException $e)
        {
            Infinity::getInstance()->log('PDOException: ' . $e->getMessage());
            throw new DB_Exception($e->getMessage());
        }

    }

    public static function exec ( $sql )
    {
        self::connect();
        return self::$connection->exec($sql);
    }

    public static function lastInsertId ()
    {
        return self::$connection->lastInsertId();
    }

    public static function beginTransaction ()
    {
        if (self::$in_transaction)
        {
            return;
        }

        self::connect();
        self::$connection->beginTransaction();
        self::$in_transaction = 1;
    }

    public static function commit ()
    {
        if (!self::$in_transaction)
        {
            return FALSE;
        }

        self::$connection->commit();
        self::$in_transaction = 0;
    }

    public static function prepare ($sql)
    {
        self::connect();
        return self::$connection->prepare($sql);
    }

    public static function create_db_from_sql ()
    {
        $schema = file_get_contents(Infinity::getInstance()->getInfinityDIR() . DIRECTORY_SEPARATOR . 'create_db.sql');
        $success = FALSE;

        try
        {
            self::exec($schema);
            $success = TRUE;
        }
        catch (\PDOException $e)
        {
            throw new DB_Exception($e->getMessage());
        }

        return $success;
    }
}

class DB_Exception extends \Exception {}
