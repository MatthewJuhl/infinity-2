<?
namespace Infinity;
use Infinity\Entity\Category, Infinity\Entity\Document, Infinity\Entity\Field;
class Content
{

    const SITE_PAGES_CATEGORY_NAME = 'Site Pages';
    private static $site_pages_category;

    public static function getSitePagesCategory ()
    {
        // TODO: add site pages category id to config
        if (!static::$site_pages_category)
        {
            $category = Category::get_by_name(static::SITE_PAGES_CATEGORY_NAME);

            if (!$category)
            {
                $category = static::createSitePagesCategory();
            }
            static::$site_pages_category = $category;
        }
        return static::$site_pages_category;
    }

    public static function createSitePagesCategory ()
    {
        $category = new Category;
        $category->name = static::SITE_PAGES_CATEGORY_NAME;
        $category->description = 'Content for individual site pages';
        if ($category->save())
        {
            return $category;
        }
        return FALSE;
    }

    public static function get_default_content_document ($page_or_sub_category_name = 'index', $page_name = NULL)
    {
        $site_pages_category = static::getSitePagesCategory();

        $sub_category_name = $page_or_sub_category_name;
        if (!$page_name)
        {
            $sub_category_name = NULL;
        }

        $document = NULL;

        $sub_category = NULL;
        if ($sub_category_name)
        {
            // we're looking for a document for /{{sub_category_name}}/{{page_name}}
            $sub_category = Category::get_by_name($sub_category_name, $site_pages_category->id);
        }
        else
        {
            // we're looking for a document for /{{page_name}}
            $sub_category = $site_pages_category;
            $page_name = $page_or_sub_category_name;
        }

        $result = NULL;

        if ($sub_category)
        {
            $document = Document::get_by_name_and_category_id($page_name, $sub_category->id);

            if ($document)
            {
                $result = $document->to_array();
                $fields = Field::where(['document_id' => $document->id]);
                foreach ($fields as $field)
                {
                    Infinity::getInstance()->log('Content field: ' . $field->name);
                    $result[$field->name] = $field->value;
                }
            }
        }

        return $result;
    }
}
