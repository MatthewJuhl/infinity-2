<?
namespace Infinity\ViewHelper;
class Input extends Tag
{
    public $tag_name = 'input';

    public function __construct ($name, $value = NULL, $type = 'text')
    {
        $this->name = $name;
        $this->id = $name;
        $this->attributes = ['type' => $type, 'value' => $value];
    }
}
