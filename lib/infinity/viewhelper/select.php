<?
namespace Infinity\ViewHelper;
class Select extends Tag
{
	public $tag_name = 'select';
	private $selected_values = [];

	public function __construct ($id, $name = NULL, $selected_values = NULL)
	{
		$this->id = $id;
		$this->name = $name;
		if (is_array($selected_values))
		{
			$this->selected_values = $selected_values;
		}
		else
		{
			$this->selected_values = [$selected_values];
		}
	}

	public function add_option ($label, $selected = FALSE, $value = NULL)
	{
		if (!isset($this->content))
		{
			$this->content = '';
		}

		$opt = new Option($label, $selected, $value);
		$this->content .= $opt->render();
	}


	public function add_options_by_objects (Array $objects)
	{
		foreach ($objects as $object)
		{
			$selected = in_array($object->id, $this->selected_values);
			$this->add_option($object->name, $selected, $object->id);
		}
	}

}
