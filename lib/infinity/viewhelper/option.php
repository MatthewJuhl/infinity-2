<?
namespace Infinity\ViewHelper;
class Option extends Tag
{
	public $tag_name = 'option';

	public function __construct ($label, $selected = FALSE, $value = NULL)
	{
		$this->content = $label;
		if ($selected)
		{
			$this->add_attribute('selected');
		}

		if ($value !== NULL)
		{
			$this->add_attribute('value', $value);
		}
	}
}
