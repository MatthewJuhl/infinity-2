<?
namespace Infinity\ViewHelper;
class Tag
{
	public $tag_name;
	public $classes = [];
	public $id;
	public $name;
	public $data = [];
	public $attributes = [];
	public $content;

	public function __toString ()
	{
		return $this->render();
	}

	public function render ()
	{
		$html = '<' . $this->tag_name;

		if (isset($this->id))
		{
			$html .= ' id="' . $this->id . '"';
		}

		if (!empty($this->classes))
		{
			$html .= ' class="';
			foreach ($this->classes AS $class_name)
			{
				$html .= "$class_name ";
			}
			$html = rtrim($html);
			$html .= '"';
		}

		if (isset($this->name))
		{
			$html .= ' name="' . $this->name . '"';
		}

		foreach ($this->data AS $key => $value)
		{
			$html .= ' data-' . $key . '="' . $value . '"';
		}

		foreach ($this->attributes as $key => $value)
		{
			if (isset($value))
			{
				$html .= ' ' . $key . '="' . $value . '"';
			}
			else
			{
				$html .= " $key";
			}

		}

		$html .= '>';

		if (isset($this->content))
		{
			$html .= $this->content . '</' . $this->tag_name . '>';
		}

		return $html;
	}

	public function add_class ($name)
	{
		$this->classes[] = $name;
	}

	public function add_data ($key, $value)
	{
		$this->data[$key] = $value;
	}

	public function add_attribute ($key, $value = NULL)
	{
		$this->attributes[$key] = $value;
	}
}
