<?
namespace Infinity\ViewHelper;
class Textarea extends Tag
{
    public $tag_name = 'textarea';

    public function __construct ($name, $value = NULL)
    {
        if (!$value)
        {
            $value = '';
        }

        $this->name = $name;
        $this->id = $name;
        $this->content = $value;
    }
}
