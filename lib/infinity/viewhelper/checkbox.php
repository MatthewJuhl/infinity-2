<?
namespace Infinity\ViewHelper;
class Checkbox extends Tag
{
    public $tag_name = 'input';

    public function __construct ($name, $checked = FALSE, $value = 1)
    {
        $this->name = $name;
        $this->attributes = ['type' => 'checkbox', 'value' => $value];
        if ($checked) $this->attributes['checked'] = 'checked';
    }
}
