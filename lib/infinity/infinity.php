<?
namespace Infinity;
class Infinity extends RequestHelper
{
	public $view;
	public $logger;
	public $content_document;

	private $config = [];
	private $routes = [ 'GET' => [], 'POST' => [] ];

	private $request_method;
	private $request_url;
	private $url_segments;
	private $controller;
	private $action;
	protected $params = [];
	private $is_cms;

	private static $instance;


	public function __construct ()
	{
		$self = $this;
		$this->view = new \Infinity\View($this);
		$this->view->set('infinity', $this);
		$this->view->set('render', function ($partial_filename, $data = []) use ($self)
		{
			return $self->view->partial($partial_filename, $data);
		});
		// initialize request handling
		$this->getRequestURL();
		$this->getRequestMethod();
		$this->getURLSegments();

		static::$instance = $this;
	}

	public function render ($view_filename)
	{
		return $this->view->render($view_filename);
	}

	public static function getInstance ()
	{
		if (!static::$instance instanceof static)
		{
			static::$instance = new static;
		}

		return static::$instance;
	}

	public function setConfig (Array $config)
	{
		foreach ($config as $key => $value)
		{
			$this->config[$key] = $value;
		}
	}

	public function getConfig ($key)
	{
		return @$this->config[$key];
	}

	# provide an interface to the internal logger
	public function log ($message)
	{
		$log_path = $this->getConfig('infinity_log_file');
		if ($log_path)
		{
			file_put_contents($log_path, $message . PHP_EOL, FILE_APPEND);
		}
	}

	public function setRequestURL ($url)
	{
		$this->request_url = $url;
	}

	public function getRequestURL ()
	{
		if (!$this->request_url)
		{
			$request_url = preg_replace('/index\.php$/', '', @$_REQUEST['infinity_request_uri']);
			$request_url = trim($request_url, '/');
			# replace dashes in url with underscores, this might always work?
			// $request_url = str_replace('-', '_', $request_url); // nope, it doesn't
			$this->request_url = $request_url;
		}

		return $this->request_url;
	}

	public function setRequestMethod ($method)
	{
		$this->request_method = $method;
	}

	public function getRequestMethod ()
	{
		if (!$this->request_method)
		{
			$this->request_method = $_SERVER['REQUEST_METHOD'];
		}

		return $this->request_method;
	}

	public function getURLSegments ($rebuild = FALSE)
	{
		$url = $this->getRequestURL();

		if (!$this->url_segments || $rebuild)
		{
			// if there's only one segment, the controller might be index
			# generate the default route from the URL (/{controller}/{action})
			$this->url_segments = explode('/', $url);
			if ($this->url_segments[0] == '') {
				# assume '/' means index/index
				$this->url_segments = ['index', 'index'];
			}
			// if (count($this->url_segments) === 1) {
				# `/{something} could infer /{controller}/index
				# or /index/{action}
				// TODO: don't decide here?
				# if assuming '/{controller}' means '/{controller}/index':
				// $this->url_segments[] = 'index';
			// }
		}

		return $this->url_segments;
	}

	public function setController ($controller_name)
	{
		# TODO: remove this
		$this->controller = $controller_name;
		$this->log('Controller set to ' . $controller_name);
		return $controller_name;
	}

	public function setAction ($action_name)
	{
		# TODO: remove this
		$this->action = $action_name;
		$this->log('Action set to ' . $action_name);
		return $action_name;
	}

	private function isCMSRequest ()
	{
		if (!isset($this->is_cms))
		{
			if ($this->getURLSegments()[0] === 'cms')
			{
				$this->log('processing as CMS request');
				$this->is_cms = TRUE;
			}
			else
			{
				$this->is_cms = FALSE;
			}
		}
		return $this->is_cms;
	}

	public function getInfinityDIR ()
	{
		return dirname(dirname(__FILE__));
	}

	public function getCMSApplicationDIR ()
	{
		return $this->getInfinityDIR() . DIRECTORY_SEPARATOR . 'cms';
	}

	public function getViewsDIR ()
	{
		return $this->getConfig('website_directory') . DIRECTORY_SEPARATOR . 'views';
	}

	public function getPartialsDIR ()
	{
		return $this->getConfig('website_directory') . DIRECTORY_SEPARATOR . 'partials';
	}

	public function getLayoutsDIR ()
	{
		return $this->getConfig('website_directory') . DIRECTORY_SEPARATOR . 'layouts';
	}

	public function getControllersDIR ()
	{
		return $this->getConfig('website_directory') . DIRECTORY_SEPARATOR . 'controllers';
	}

	private function auto_load_content ()
	{
		if ($this->is_cms)
		{
			// no default content for CMS pages
			return;
		}

		if ($this->getConfig('auto_load_content'))
		{
			$this->log('Autoloading content document');
			# by default, try to load the document titled {action} in the category {controller}

			$segments = $this->getURLSegments();

			$this->content_document = Content::get_default_content_document(@$segments[0], @$segments[1]);
			if ($this->content_document)
			{
				$this->log('Content document loaded.');
			} else
			{
				$this->log('No content document found.');
			}
		}
	}

	private function is_file_a_controller ($filename)
	{
		return preg_match('/\-controller\.php$/', $filename);
	}

	protected function load_all_controllers ()
	{
		$controllers_dir_path = $this->getControllersDIR();
		$controllers_dir = opendir($controllers_dir_path);

		while ($file = readdir($controllers_dir))
		{
			if ($this->is_file_a_controller($file))
			{
				$this->log('including controller: ' . $file);
				include_once $controllers_dir_path . DIRECTORY_SEPARATOR . $file;
			}
		}

		closedir($controllers_dir);
	}

	public function launch ()
	{
		$url = $this->getRequestURL();
		$method = $this->getRequestMethod();
		$url_segments = $this->getURLSegments();
		$this->log(PHP_EOL . PHP_EOL . 'PROCESSING REQUEST: ' . $method . ' /' . $url);

		# Redirect all URLs beginning with CMS to the CMS
		if ($this->isCMSRequest())
		{
			require 'cms/cms.php';
			# rebuild the request
			$url = $this->getRequestURL();
			$method = $this->getRequestMethod();
			$url_segments = $this->getURLSegments();
		}

		# load all the controller files
		$this->load_all_controllers();

		# look for a route controller for this URL
		$route_controller_match = $this->route($url, $method);

		# check for an authenticated user in the session
		$user = Auth::get_signed_in_user();

		# if this is a CMS request, require authentication
		if ($this->isCMSRequest() && (!$user || !$user->is_cms_user()))
		{
			$this->log('Access to CMS is denied. Authentication is required.');
			$this->require_sign_in('/cms/user/sign-in', FALSE, 'cms/' . $url);
			# if the user is not authenticated, the user will be redirected and this request dies here
		}

		#magic
		# if configured, automatically load content and attach it to the view
		$this->auto_load_content();
		$this->view->set('content', $this->content_document);
		$this->view->set('params', $this->params);

		# execute the route controller function if one was found
		if ($route_controller_match)
		{
			$this->log('Executing route controller.');
			$this->params = $route_controller_match['params'];
			$route_controller_match['controller_function']($this, $this->params);
		}
		else
		{
			$this->log('No route controller was found.');
		}

		#magic
		# if a view hasn't been rendered, try rendering one automatically based
		# on the first two URL segments
		if (!$this->view->getIsRendered())
		{
			# look for a view file
			$view_file = $url_segments[0];
			if (count($url_segments) > 1)
			{
				$view_file .= DIRECTORY_SEPARATOR . $url_segments[1];
			}
			$this->log('looking for view file: ' . $view_file);
			# suppressing warnings here allows warnings to be raised when a user
			# explicitly calls render
			@$this->view->render($view_file);
		}

		$this->log('DONE.' . PHP_EOL);
	}
}
