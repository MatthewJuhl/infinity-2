<?
namespace Infinity;
abstract class RequestHelper extends RouteHelper
{

	const SESSION_PREFIX = 'Infinity.';

	function redirect ($url)
	{
		$this->log('redirecting to ' . $url);
		header('location: ' . $url);
		die();
	}

	# continue to another route during the current request
	function forward ($url, $method = NULL)
	{
		if (!$method)
		{
			$method = $this->getRequestMethod();
		}

		$this->log("Forwarding request to: $method $url");

		# look for a route controller for this URL
		$route_controller_match = $this->route($url, $method);

		// TODO: check for CMS route?

		# execute the route controller function if one was found
		if ($route_controller_match)
		{
			$this->log('Executing forwarded route controller.');
			$this->params = array_merge($this->params, $route_controller_match['params']);
			$route_controller_match['controller_function']($this, $this->params);
		}
		else
		{
			$this->log('No route controller was found.');
		}
	}

	function set_session ($key, $val)
	{
		@session_start();
		$_SESSION[self::SESSION_PREFIX . $key] = $val;
	}

	function delete_session ($key)
	{
		@session_start();
		unset($_SESSION[self::SESSION_PREFIX . $key]);
	}

	function get_session ($key)
	{
		@session_start();
		return @$_SESSION[self::SESSION_PREFIX . $key];
	}

	function get_post ($post = NULL, $no_filter = FALSE)
	{
		$post OR $post = $_POST;
		if (!$no_filter)
		{
			// TODO: figure out default post filtering
		}
		return $post;
	}

	function get_missing_fields ($required, $post = NULL)
	{
		$this->log("checking post for required fields");
		$post OR $post = $this->get_post();

		$missing = [];
		foreach ($required as $name)
		{
			$this->log("checking $name");
			# check that it's set and a truthy value
			if (!array_key_exists($name, $post) || !$post[$name])
			{
				$missing[] = $name;
			}
		}

		return $missing;
	}

	function save_form ($form = NULL)
	{
		$form OR $form = $this->get_post();
		$this->set_session('saved_form', $form);
	}

	function get_form ()
	{
		$form = $this->get_session('saved_form');
		if (!$form)
		{
			$form = [];
		}
		$this->delete_session('saved_form');
		return $form;
	}

	function clear_form ()
	{
		$this->get_form();
	}

	function require_https ()
	{
		if ($_SERVER['HTTPS'] != 'on')
		{
			header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
			exit();
		}
	}

	function require_sign_in ($sign_in_url = '/sign-in', $is_signed_in = NULL, $request_url = NULL)
	{
		if ($request_url === NULL)
		{
			$request_url = $this->getRequestURL();
		}

		# redirect loops are bad
		if (ltrim($sign_in_url, '/') === $request_url) return;

		if (!isset($is_signed_in))
		{
			$is_signed_in = Auth::is_signed_in();
		}
		if (!$is_signed_in)
		{
			$this->log('Setting sign-in callback_url to ' . $_SERVER['REQUEST_URI']);
			$this->set_session('callback_url', $_SERVER['REQUEST_URI']);
			$this->redirect($sign_in_url);
		}
	}

	function get_token ()
	{
		$session_id = session_id();
		$token = '';
		if (!empty($session_id) && strlen($session_id) > 8)
		{
			$token .= substr($session_id, strlen($session_id) - 4, strlen($session_id));
			$token .= substr($session_id, 0, 4);
		}
		return $token;
	}

	function get_token_parameter_name ()
	{
		return 'inftoken';
	}

	function get_token_parameter ()
	{
		return $this->get_token_parameter_name() . '=' . $this->get_token();
	}

	function require_token ()
	{
		// validate or die!
		// prevents cross site request forgery
		$token = $this->get_token();
		if (@$_GET[$this->get_token_parameter_name()] !== $token)
		{
			$this->log('Token missing for request: ' . $_SERVER['REQUEST_METHOD'] . ' ' . $_SERVER['REQUEST_URI']);

			// TODO: handle this more cleanly
			@ob_clean();
			header("HTTP/1.1 403 Unauthorized");
			echo '<h1>Unauthorized</h1>';
			die();
		}
	}

	function get_flash ()
	{
		$flash = $this->get_session('flash');
		if (!$flash)
		{
			$flash = [];
		}
		$this->delete_session('flash');
		return $flash;
	}

	function set_flash ($flash)
	{
		$this->set_session('flash', $flash);
	}

	function add_flash ($message, $type = NULL)
	{
		$flash = $this->get_flash();
		$flash[] = array('message' => $message, 'type' => $type);
		$this->set_flash($flash);
	}

}
