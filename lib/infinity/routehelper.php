<?
namespace Infinity;
abstract class RouteHelper
{
	private $custom_routes = [];
	// [request_method, url_pattern, controller_function]

	static function isSegmentVariable ($segment)
	{
		return preg_match('/\{\{/', $segment);
	}

	# warning, this will delete existing routes
	public function defineRoutes ($route_array)
	{
		$this->custom_routes = $route_array;
	}

	public function addRoute ($method, $url_pattern, $controller_function)
	{
		$this->custom_routes[] = [$method, $url_pattern, $controller_function];
	}

	public function route ($url = NULL, $method = NULL)
	{

		if (!$method)
		{
			$method = $_SERVER['REQUEST_METHOD'];
		}

		if (!$url) {
			// TODO: what is this line doing?
			$url = preg_replace('/\?.+$/', '', $_SERVER['REQUEST_URI']);
			// TODO: is it supposed to be doing this?:
			$url = rtrim(ltrim($url, '/'), '/'); # <-- this one works!
		}

		$url_array = explode('/', $url);
		$url_array_count = count($url_array);

		// an associative array to store the values of route segment variables
		$params = array();

		// test each defined route to see if the URL matches the route
		foreach ($this->custom_routes AS $route)
		{
			list($route_method, $route_pattern, $route_controller) = $route;

			// method must match or be a wildcard
			if ($route_method === '*' || strtolower($route_method) === strtolower($method))
			{
				// break up the route's pattern by `/`
				$route_url_array = explode('/', ltrim($route_pattern, '/'));

				// check if the number of segments match
				if (count($route_url_array) === $url_array_count)
				{
					// assume the route segment matches the url segment
					$segments_match = 1;
					// now try to disprove it:
					foreach($route_url_array AS $index => $segment)
					{
						// check if the current segment is a variable, eg {{example}}
						if (self::isSegmentVariable($segment))
						{
							// the route segment is a variable, so it counts as a match
							// store the value to the params array
							$params[preg_replace(array('/\{\{/','/\}\}/'), '',$segment)] = urldecode($url_array[$index]);
						}
						elseif ($segment === $url_array[$index])
						{
							// the route segment and the corresponding url segment are exactly the same
							// nothing needs to happen here, it's just for human intelligibility
						}
						else
						{
							// the route segment is not a variable, and it does not match the URL segment
							// therefore, the URL does not match this route
							$segments_match = 0;
							break;
						}
					}

					if ($segments_match)
					{
						$this->log('URL matches route: ' . $route_pattern);
						return [
							'controller_function' => $route_controller,
							'params' => $params
						];
					}
				}
			}
		}
		return FALSE;
	}
}
