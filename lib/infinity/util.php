<?
namespace Infinity;
class Util
{
    // converts underscores to dashes
    static function dasherize ($string)
    {
        return str_replace('_', '-', $string);
    }
}
