<?
namespace Infinity\Entity;
class FieldDraft extends Base
{
    public $id; // I wimped out. This just makes things so easy.
    public $document_id;
    public $field_id;
    public $value;

    public $created;
    public $modified;
    public $created_by_user_id;
    public $modified_by_user_id;

    static function get_by_field_id ($field_id)
    {
        $field_draft = FALSE;
        $draft_fields = self::where(['field_id' => $field_id]);
        if (count($draft_fields))
        {
            $field_draft = $draft_fields[0];
        }
        return $field_draft;
    }
}
