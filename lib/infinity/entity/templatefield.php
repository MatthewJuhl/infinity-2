<?
namespace Infinity\Entity;
class TemplateField extends Base
{
	public $id;
	public $template_id;
	public $name;
	public $description;
	public $format;
	public $default_value;
	public $sort_order;

	public $created;
	public $modified;
	public $created_by_user_id;
	public $modified_by_user_id;

	public static function sort (&$array)
	{
		$i = \Infinity\Infinity::getInstance();
		$i->log('sorting fields');


		// sorts an array of fields based on their sort_order property
		usort($array, function ($a, $b) use ($i)
		{
			if (is_numeric($a->sort_order) && is_numeric($b->sort_order))
			{
				$i->log('both have a sort order');
				$i->log($a->sort_order . ' vs ' . $b->sort_order);
			// both a and b have values, so compare them
				// -1 means flip put b first
				// 1 means put a first
				// 0 means leave them as is
				if ($a->sort_order == $b->sort_order)
				{
					$i->log('they are equal');
					return 0;
				}
				return ($a->sort_order < $b->sort_order) ? -1 : 1;
			}
			elseif (is_numeric($a->sort_order))
			{
			// a has a value, but b doesn't, so a goes first
				return -1;
			}
			elseif (is_numeric($b->sort_order))
			{
			// b has a value, but a doesn't, so b goes first
				return 1;
			}
			else
			{
				return 0; // neither a nor b have a value
			}

		});
	}
}
