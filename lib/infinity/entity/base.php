<?php
namespace Infinity\Entity;
use Infinity\Infinity, Infinity\DB;
class Base
{
    private static $_has_connection;

    // TODO: move this to db.php
    private static function require_data_connection ()
    {
        if (self::$_has_connection) {
            return;
        }

        if (!file_exists(DB::get_db_filename()))
        {
            static::build_db();
        }
        DB::connect();
    }

    // TODO: move this to db.php
    private static function build_db ()
    {
        $infinity = Infinity::getInstance();
        $infinity->log('Building site database from scratch.');
        DB::create_db_from_sql();
        $infinity->log('Creating admin user.');
        $developer_email = $infinity->getConfig('developer_email');
        $infinity->log('developer email: ' . $developer_email);
        $user = User::create($developer_email, 'admin', 'Lead Developer', User::TYPE_DEVELOPER);
        $infinity->log('admin user created: ' . print_r($user, 1));
    }

    static function get_table_name ()
    {
        $classname = get_class(new static);
        if (preg_match('@\\\\([\w]+)$@', $classname, $matches)) {
            $classname = $matches[1];
        }
        return $classname;
    }

    function populate ($row)
    {
        foreach ($this AS $key => $val) {
            if (array_key_exists($key, $row))
            {
                $this->$key = @$row[$key];
            }

        }
    }

    public function __construct ($row = NULL) {
        if ($row)
        {
            $this->populate($row);
        }

        return $this;
    }

    // safely sets properties only if they exist in the post array
    // (properties will be set if they are NULL or empty)
    // this means properties won't get obliterated if they're ommitted from the post
    public function set_if_posted ($property_name, $post_array = NULL)
    {
        if (NULL === $post_array)
        {
            $post_array = $_POST;
        }

        if (property_exists($this, $property_name) && array_key_exists($property_name, $post_array))
        {
            $this->$property_name = $post_array[$property_name];
        }
    }

    public function check_permission ()
    {
        if (TRUE) {
            return TRUE;
        }
    }

    public function insert ()
    {
        $this->check_permission();
        $this->before_create();
        $this->before_save();
        try {
            $keys = '';
            $vals = '';

            $now = time();
            if (property_exists($this, 'created'))
            {
                $this->created = $now;
            }

            // if (property_exists($this, 'modified'))
            // {
            //  $this->modified = $now;
            // }

            $bound_vals = array();

            foreach ($this AS $key => $val) {
                $keys .= $key . ', ';
                $vals .= ':' . $key . ', ';
                $bound_vals[$key] = $val;
            }
            $keys = rtrim($keys, ', ');
            $vals = rtrim($vals, ', ');
            $sql = 'INSERT INTO ' . static::get_table_name() . ' (' . $keys . ") \nVALUES ($vals)";


            $statement = DB::prepare($sql);
            if (!$statement)
            {
                throw new Entity_Exception('Statement is empty for query: ' . $sql);
            }

            $statement->execute($bound_vals);

            if ($statement->rowCount() < 1) {
                return FALSE;
            }

            if (property_exists(new static, 'id'))
            {
                $this->id = DB::lastInsertId();
            }

            $this->after_create();
            $this->after_save();

            return TRUE;
        } catch (\PDOException $e) {
            throw new Entity_Exception($e->getMessage());
        }
    }

    public static function get_where_clause ()
    {
        return 'id = :id';
    }

    public function get_where_params ()
    {
        return array('id' => $this->id);
    }

    public function update ()
    {
        $this->check_permission();
        $this->before_update();
        $this->before_save();
        try {
            $sql = 'UPDATE ' . static::get_table_name() . "\nSET ";

            $now = time();

            if (property_exists($this, 'modified'))
            {
                $this->modified = $now;
            }

            $vals = array();

            foreach ($this AS $key => $val)
            {
                $vals[$key] = $val;
                if ($key != 'id')
                {
                    // don't try to update the id
                    $sql .= "$key = :$key, ";
                }
            }

            $sql = rtrim($sql,', ');
            $where = static::get_where_clause();
            $sql .= "\nWHERE $where";

            $statement = DB::prepare($sql);
            if (!$statement)
            {
                throw new Entity_Exception('Statement is empty for query: `' . $sql . '`. This might be a result of the entity class definition being out of date with the table.');
            }
            $statement->execute($vals);

            if ($statement->rowCount() < 1) {
                return FALSE;
            }

            $this->after_update();
            $this->after_save();
            return TRUE;

        } catch (PDOException $e) {
            // TODO:
            die($e->getMessage());
        }
    }


    public function save ()
    {
        // if the primary key isn't `id` we can't immediately infer whether to insert or update
        if (!property_exists(new static, 'id'))
        {
            throw new Entity_Exception('Entities without an `id` property must explicitly call insert or update.');
        }


        if (empty($this->id) || $this->id <= 0)
        {
            return $this->insert();
        }
        else
        {
            return $this->update();
        }

    }


    public function delete ()
    {
        $this->check_permission();
        $this->before_delete();
        $where = static::get_where_clause();

        $sql = 'DELETE FROM ' . static::get_table_name() . " WHERE $where";
        try {
            $statement = DB::prepare($sql);
            $where_params = $this->get_where_params();
            $statement->execute($where_params);

            return true;
        }
        catch (PDOException $e) {
            // TODO:
            die($e->getMessage());
        }
        $this->after_delete();
    }


    public static function get ($params)
    {
        $where = static::get_where_clause();
        $sql = 'SELECT * FROM ' . static::get_table_name() . " WHERE $where";



        if (!is_array($params))
        {
            $params = array('id' => $params);
        }

        $instance = new static;
        try {
            $statement = DB::prepare($sql);
            if (!$statement)
            {
                throw new Entity_Exception('Statement is empty for query: `' . $sql . '`');
            }
            $statement->execute($params);

            $row = $statement->fetch();
            if (!empty($row)) {

                $instance->populate($row);
                return $instance;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            throw new Entity_Exception($e->getMessage());
        }
    }


    public static function where ($criteria)
    {
        self::require_data_connection();
        // TODO: test this function
        $table_name = static::get_table_name();

        $sql = 'SELECT * FROM ' . $table_name . " \nWHERE ";

        $count = count($criteria);

        foreach ($criteria AS $key => $val) {
            $sql .= "$key = :$key AND ";
        }

        $sql = preg_replace('/ AND $/', '', $sql);

        Infinity::getInstance()->log('Running query: ' . $sql);
        Infinity::getInstance()->log('with params: ' . print_r($criteria, 1));

        $results = array();
        try {

            $instance = new static;
            $statement = DB::prepare($sql);
            if (!$statement)
            {
                throw new Entity_Exception('Statement is empty for query: ' . $sql);
            }
            $statement->execute($criteria);

            while ($row = $statement->fetch()) {
                $instance = new static;
                $instance->populate($row);
                $results[] = $instance;
            }

            return $results;
        }
        catch (PDOException $e) {
            die($e->getMessage());
        }
    }



    public static function all ()
    {
        self::require_data_connection();
        $table_name = static::get_table_name();

        $sql = 'SELECT * FROM ' . $table_name;


        $results = array();
        try {


            $statement = DB::prepare($sql);
            $statement->execute();

            while ($row = $statement->fetch()) {
                $instance = new static;
                $instance->populate($row);
                $results[] = $instance;
            }

            return $results;
        }
        catch (PDOException $e) {
            // TODO:
            die($e->getMessage());
        }
    }

    public function to_array () {
        $array = [];
        foreach ($this AS $key => $value)
        {
            $array[$key] = $value;
        }
        return $array;
    }

    public static function make_associative (Array $list)
    {
        $results = [];
        foreach ($list as $entity)
        {
            $results[$entity->id] = $entity;
        }
        return $results;
    }

    function before_create () {}
    function before_update () {}
    function before_save () {}
    function after_create () {}
    function after_update () {}
    function after_save () {}
    function before_delete () {}
    function after_delete () {}
}

class Entity_Exception extends \Exception {}
