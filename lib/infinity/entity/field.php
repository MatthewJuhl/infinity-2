<?
namespace Infinity\Entity;
class Field extends Base
{
	public $id;
	public $document_id;
	public $name;
	public $description;
	public $format = self::FORMAT_SHORT_TEXT;
	public $value;
	public $template_field_id;
	public $created;
	public $modified;
	public $created_by_user_id;
	public $modified_by_user_id;

	const FORMAT_SHORT_TEXT = 1; // single line string
	const FORMAT_PLAIN_TEXT = 2; // nl2br(string)
	const FORMAT_CHECKBOX = 11;


	private static $format_names = [
		self::FORMAT_SHORT_TEXT => 'short-text',
		self::FORMAT_PLAIN_TEXT => 'plain-text',
		self::FORMAT_CHECKBOX => 'checkbox'
	];

	static function get_format_name ($format)
	{
		return self::$format_names[$format];
	}

	public static function interpret_posted_value ($value, $format)
	{
		switch ($format)
		{
			case self::FORMAT_CHECKBOX:
				return !!$value;
				break;
			default:
				return $value;
		}
	}

	public static function associate_by_template_field_id (Array $fields)
	{
		$results = [];

		foreach ($fields as $field)
		{
			$results[$field->template_field_id] = $field;
		}

		return $results;
	}

	// static function get_by_name ($name, $parent_category_id = NULL)
	// {
	//  return self::where(['name'] => $name, 'parent_category_id' => $parent_category_id]);
	// }
}
