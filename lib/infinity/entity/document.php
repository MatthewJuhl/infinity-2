<?
namespace Infinity\Entity;
use \Infinity\DB;
class Document extends Base
{
	public $id;
	public $name;
	public $description;
	public $category_id;
	public $published;
	public $template_id;
	public $created;
	public $modified;
	public $created_by_user_id;
	public $modified_by_user_id;

	static function get_by_name_and_category_id ($name, $category_id = NULL)
	{
		try {
			return @self::where(['name' => $name, 'category_id' => $category_id])[0];
		}
		catch (Exception $e) {
			return NULL;
		}
	}

	public function get_fields ()
	{
		// TODO: save this on the object
		return Field::where(['document_id' => $this->id]);
	}

	public function init_template ()
	{

		$template = Template::get($this->template_id);
		// TODO: error if template doesn't exist
		$template_fields = TemplateField::where(['template_id' => $template->id]);

		DB::beginTransaction();

		foreach ($template_fields as $template_field)
		{
			$field = new Field;
			$field->template_field_id = $template_field->id;
			$field->document_id = $this->id;
			$field->name = $template_field->name;
			$field->description = $template_field->description;
			$field->format = $template_field->format;
			$field->value = $template_field->default_value;
			$field->save();
		}

		DB::commit();
	}

	public function after_create ()
	{
		$this->init_template();
	}
}
