<?
namespace Infinity\Entity;
use Infinity\Security;
class User extends Base
{
    public $id;
    public $email_address;
    public $password;
    public $name;
    public $type;

    public $created;
    public $modified;

    public $is_active;

    # define some basic permission levels
    const TYPE_DEVELOPER = 100; // god mode
    const TYPE_ADMINISTRATOR = 90;
    const TYPE_EDITOR = 80;
    const TYPE_STAFF = 70; // lowest level of CMS access
    const TYPE_USER = 60;

    public function has_access ($level)
    {
        return $this->type >= $level;
    }

    public function is_cms_user ()
    {
        # all cms users are >=70
        return $this->has_access(USER::TYPE_STAFF);
    }

    public static function get_by_email ($email_address)
    {
        $array = self::where(array('email_address' => $email_address));

        if (count($array)) {
            return $array[0];
        }
        return FALSE;
    }

    public static function create ($email_address, $password, $name, $type = self::TYPE_USER, $is_active = 1)
    {
        $existing_user = self::get_by_email($email_address);
        if ($existing_user instanceof User)
        {
            throw new UserException('The email address provided is already in use.');
            return;
        }


        $user = new User;
        $user->email_address = $email_address;
        $user->name = $name;
        $user->type = $type;
        $user->is_active = $is_active;
        $user->password = Security::hash_password($password);
        $success = $user->save();

        if (!$success)
        {
            throw new UserException('Could not save user to database.');
        }

        return $user;
    }
}

class UserException extends \Exception {}
