<?
namespace Infinity\Entity;
class Template extends Base
{
    public $id;
    public $name;
    public $description;
    public $created;
    public $modified;
    public $created_by_user_id;
    public $modified_by_user_id;
}
