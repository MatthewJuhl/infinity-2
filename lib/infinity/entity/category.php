<?
namespace Infinity\Entity;
use Infinity\Infinity;
class Category extends Base
{
	public $id;
	public $name;
	public $description;
	public $parent_category_id;
	public $template_id;
	public $created;
	public $modified;
	public $created_by_user_id;
	public $modified_by_user_id;

	public function get_children ()
	{
		return static::where(['parent_category_id' => $this->id]);
	}

	static function get_category_tree ()
	{
		$all_categories = static::all();
		$tree = static::filter_by_parent_category_id($all_categories, NULL);
		if (!empty ($tree))
		{
			static::build_tree($tree, $all_categories);
		}

		return $tree;
	}
	// takes an array of categories ("branch") and attaches child categories to each
	protected static function build_tree (&$categories, $all_categories)
	{
		foreach ($categories as $category)
		{
			$category->child_categories = static::filter_by_parent_category_id($all_categories, $category->id);
			// recurse
			if (!empty($category->child_categories))
			{
				static::build_tree($category->child_categories, $all_categories);
			}
		}
	}

	static function filter_by_parent_category_id ($list, $parent_category_id = NULL)
	{
		return array_filter($list, function ($category) use ($parent_category_id)
		{
			return $category->parent_category_id == $parent_category_id;
		});
	}

	static function get_by_name ($name, $parent_category_id = NULL)
	{
		$params = ['name' => $name];
		if ($parent_category_id)
		{
			$params['parent_category_id'] = $parent_category_id;
		}
		try
		{
			return @self::where($params)[0];
		}
		catch (Exception $e)
		{
			return NULL;
		}
	}

	static function get_category_name_map ($all_categories = NULL)
	{
		if (!$all_categories)
		{
			$all_categories = self::all();
		}

		$all_categories = self::make_associative($all_categories);

		$results = [];
		$single_generation = [];

		// start by getting each category's own name
		foreach ($all_categories as $category)
		{
			$single_generation[$category->id] = $category->name;
		}

		$x = \Infinity\Infinity::getInstance();
		$x->log('single gen cat tree build: ' . print_r($single_generation, 1));
		$x->log('all == ' . print_r($all_categories, 1));

		// then, append all generations of parents' names to the category
		foreach ($all_categories as $category)
		{
			if (is_numeric($category->parent_category_id))
			{
				$x->log($category->name . ' has a parent');
				self::rename_category_by_parent($category, $all_categories[$category->parent_category_id], $single_generation, $all_categories);
			}

			$results[$category->id] = $category->name;
		}


		return $results;
	}

	private static function rename_category_by_parent (&$category, $parent, $single_generation_name_map, $all_categories_associative)
	{
		$x = \Infinity\Infinity::getInstance();
		$x->log('renaming ' . $category->name . ' by ' . print_r($parent, 1));
		$category->name = $single_generation_name_map[$parent->id] . ' // ' . $category->name;
		if (is_numeric($parent->parent_category_id))
		{
			$grandparent = $all_categories_associative[$parent->parent_category_id];
			self::rename_category_by_parent($category, $grandparent, $single_generation_name_map, $all_categories_associative);
		}
	}
}
