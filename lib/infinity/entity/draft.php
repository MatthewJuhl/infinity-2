<?
namespace Infinity\Entity;
use \Infinity\DB;
class Draft extends Base
{
	public $id; // references Document
	public $created;
	public $modified;
	public $created_by_user_id;
	public $modified_by_user_id;

	private $document;
	private $template;
	private $document_fields;
	private $fields; // draft versions

	public function get_fields ()
	{
		if (!$this->fields)
		{
			$this->fields = FieldDraft::where(['document_id' => $this->id]);
		}
		return $this->fields;
	}

	public function get_document ()
	{
		if (!$this->document)
		{
			$this->document = Document::get($this->id);
		}
		return $this->document;
	}

	public function get_template ()
	{
		$doc = $this->get_document();
		if (!$this->template)
		{
			$this->template = Template::get($document->template_id);
		}
		return $this->template;
	}

	public function after_create ()
	{
		$this->init_draft_fields();
	}

	private function init_draft_fields ()
	{
		$document = $this->get_document();
		$document_fields = $document->get_fields();

		DB::beginTransaction();

		foreach ($document_fields as $document_field)
		{
			$field_draft = new FieldDraft;

			$field_draft->document_id = $document->id;
			$field_draft->field_id = $document_field->id;
			$field_draft->value = $document_field->value;
			$field_draft->save();
		}

		DB::commit();
	}

	public function before_delete ()
	{
		foreach($this->get_fields() as $field)
		{
			$field->delete();
		}
	}
}
