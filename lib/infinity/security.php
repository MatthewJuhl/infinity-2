<?
namespace Infinity;
class Security
{
    /* based on http://stackoverflow.com/questions/10916284/how-to-encrypt-decrypt-data-in-php <18 Apr 2013> */

    static function generate_encryption_key ($length = 32)
    {
        $strong = FALSE;
        $key = openssl_random_pseudo_bytes($length, $strong);
        if (!$strong)
        {
            throw new SecurityException('Failed to generate a safe encryption key.');
        }
        return $key;
    }

    static function encrypt ($string, $encryption_key, $base64_encode = TRUE)
    {
        if (!$encryption_key)
        {
            throw new SecurityException('Missing encryption key');
        }
        $encrypted_value = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $encryption_key, $string, MCRYPT_MODE_CFB, substr(md5($encryption_key), 0, 16));
        return $encrypted_value;
    }

    static function decrypt ($string, $encryption_key, $base64_decode = TRUE)
    {
        $unencrypted_value = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $encryption_key, $string, MCRYPT_MODE_CFB, substr(md5($encryption_key), 0, 16));
        return $unencrypted_value;
    }

    static function hash_password ($password)
    {
        $salt = strtr(base64_encode(openssl_random_pseudo_bytes(18)), '+', '.');
        return crypt($password, sprintf('$2y$%02d$%s', 13, $salt));
    }

    /* sample usage:
     *  $is_valid_password = Security::validate_password($_POST['password'], $row['password_hash']);
     */
    static function is_valid_password ($password, $hashed_password)
    {
        $given_hash = crypt($password, $hashed_password);
        return self::is_equal($given_hash, $hashed_password);
    }

    private static function is_equal ($str1, $str2)
    {
        $n1 = strlen($str1);
        if (strlen($str2) != $n1) {
            return false;
        }
        for ($i = 0, $diff = 0; $i != $n1; ++$i) {
            $diff |= ord($str1[$i]) ^ ord($str2[$i]);
        }
        return !$diff;
    }
}

class SecurityException extends \Exception {}
