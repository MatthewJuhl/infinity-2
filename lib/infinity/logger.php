<?
namespace Infinity;
class Logger implements LoggerInterface
{

    private $log_file_path;

    public function __construct ($path)
    {
        $this->log_file_path = $path;
    }

    public function log ($message)
    {
        file_put_contents($this->log_file_path, $message . PHP_EOL, FILE_APPEND);
    }
}
