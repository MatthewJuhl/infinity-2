<?
namespace Infinity;
class View
{
    private $view_variables;
    private $layout_filename;
    private $infinity_instance;
    private $view_filename;

    private $is_rendered = FALSE;

    function __construct (&$infinity)
    {
        $this->infinity_instance = $infinity;
        $this->view_variables = [];
    }

    public function set ($key, $value = NULL)
    {
        if (is_array($key))
        {
            foreach ($key as $akey => $avalue)
            {
                $this->view_variables[$akey] = $avalue;
            }
        }
        else $this->view_variables[$key] = $value;
    }

    public function setLayout ($layout_filename)
    {
        $this->layout_filename = $layout_filename;
    }

    public function render ($view_filename)
    {
        if (isset($this->layout_filename)) {
            $this->infinity_instance->log('layout detected: ' . $this->layout_filename);
            # if using a layout, output the layout now, and save the view for
            # rendering when specified by the layout file
            $this->view_filename = $view_filename;
            $layout_filename = $this->layout_filename;
            $this->layout_filename = NULL;

            $full_layout_path = $this->infinity_instance->getLayoutsDIR() . DIRECTORY_SEPARATOR;
            $full_layout_path .= $layout_filename . '.phtml';

            $layout_variables = $this->view_variables;
            $self = $this;
            $layout_variables['view'] = function () use (&$self, $view_filename)
            {
                $self->render($view_filename);
            };

            if (!static::output($full_layout_path, $layout_variables))
            {
                // TODO: handle layout file not found
                $this->infinity_instance->log("Layout file not found. ($full_layout_path)");
            }
        } else {
            $this->infinity_instance->log('rendering view');
            $this->is_rendered = TRUE;

            $full_view_path = $this->infinity_instance->getViewsDIR() . DIRECTORY_SEPARATOR;
            $full_view_path .= $view_filename . '.phtml';

            if (!static::output($full_view_path, $this->view_variables))
            {   // TODO: handle view file not found
                $this->infinity_instance->log("View file not found. ($full_view_path)");
                $not_found_view_path = $this->infinity_instance->getViewsDIR() . DIRECTORY_SEPARATOR . 'error/404.phtml';
                if (!static::output($not_found_view_path, $this->view_variables))
                {
                    $this->infinity_instance->log("404 view not found. Expected at: $not_found_view_path");
                    echo '<h1>404</h1>';
                }
            }
        }
    }

    public function partial ($partial_filename, $data = array()) {
        $full_path = $this->infinity_instance->getPartialsDIR() . DIRECTORY_SEPARATOR;
        $full_path .= $partial_filename . '.phtml';

        if (!static::output($full_path, $data))
        {
            // TODO: handle not found
        }
    }

    private static function output (/*$filename, $variables*/)
    {

        # extract variables for within the view file
        extract(func_get_arg(1));
        Infinity::getInstance()->log('here56679999');
        return include func_get_arg(0);
    }

    public function getIsRendered ()
    {
        return $this->is_rendered;
    }

    public function redirect ($where)
    {
        header('location: ' . $where);
        die();
    }
}
