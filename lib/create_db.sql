CREATE TABLE IF NOT EXISTS User (
    id            INTEGER PRIMARY KEY,
    email_address TEXT NOT NULL COLLATE NOCASE,
    password      TEXT,
    name          TEXT,
    type          INTEGER,
    created       INTEGER,
    modified      INTEGER,
    is_active     INTEGER
);
CREATE UNIQUE INDEX UserEmailAddressIndex ON User(email_address);

CREATE TABLE IF NOT EXISTS Document (
    id INTEGER PRIMARY KEY,
    name TEXT COLLATE NOCASE,
    description TEXT,
    category_id INTEGER REFERENCES Category,
    published INTEGER,
    template_id INTEGER REFERENCES Template,
    created INTEGER,
    modified INTEGER,
    created_by_user_id INTEGER REFERENCES User,
    modified_by_user_id INTEGER REFERENCES User
);
CREATE INDEX DocumentCategoryIdIndex ON Document(category_id);
CREATE UNIQUE INDEX CategoryDocumentNameIndex ON Document(category_id, name); -- document name must be unique per category

CREATE TABLE IF NOT EXISTS Draft (
    id INTEGER PRIMARY KEY,
    document_id INTEGER REFERENCES Document,
    created INTEGER,
    modified INTEGER,
    created_by_user_id INTEGER REFERENCES User,
    modified_by_user_id INTEGER REFERENCES User
);
CREATE UNIQUE INDEX DraftDocumentIdIndex ON Draft(document_id);


-- TODO: some columns ought to be removed in favor of deferring to the template
CREATE TABLE IF NOT EXISTS Field (
    id INTEGER PRIMARY KEY,
    document_id INTEGER REFERENCES Document,
    template_field_id INTEGER REFERENCES TemplateField,
    name TEXT COLLATE NOCASE, -- TODO: remove?
    description TEXT, -- TODO: remove?
    format INTEGER, -- TODO: remove?
    reference_category_id REFERENCES Category, -- TODO: remove?
    value BLOB,
    created INTEGER,
    modified INTEGER,
    created_by_user_id INTEGER REFERENCES User,
    modified_by_user_id INTEGER REFERENCES User
);
CREATE INDEX FieldDocumentIdIndex ON Field(document_id);
CREATE UNIQUE INDEX DocumentFieldNameIndex ON Field(document_id, name); -- field name must be unique per document

-- this table stores draft versions of existing Fields
CREATE TABLE IF NOT EXISTS FieldDraft (
    id INTEGER PRIMARY KEY,
    document_id INTEGER REFERENCES Document,
    field_id INTEGER REFERENCES Field,
    value BLOB,
    created INTEGER,
    modified INTEGER,
    created_by_user_id INTEGER REFERENCES User,
    modified_by_user_id INTEGER REFERENCES User
);
CREATE UNIQUE INDEX DocumentFieldDraftIndex ON FieldDraft(document_id, field_id);

CREATE TABLE IF NOT EXISTS Category (
    id INTEGER PRIMARY KEY,
    name TEXT COLLATE NOCASE,
    description TEXT,
    parent_category_id INTEGER REFERENCES Category, -- leave null for top level category
    template_id INTEGER REFERENCES Template, -- default template for this category
    created INTEGER,
    modified INTEGER,
    created_by_user_id INTEGER REFERENCES User,
    modified_by_user_id INTEGER REFERENCES User
);
CREATE INDEX CategoryParentCategoryIdIndex ON Category(parent_category_id);
CREATE UNIQUE INDEX CategoryNameIndex ON Category(name, parent_category_id); -- category name must be unique per sub category

CREATE TABLE IF NOT EXISTS Template (
    id INTEGER PRIMARY KEY,
    name TEXT COLLATE NOCASE,
    description TEXT,
    created INTEGER,
    modified INTEGER,
    created_by_user_id INTEGER REFERENCES User,
    modified_by_user_id INTEGER REFERENCES User
);
CREATE UNIQUE INDEX TemplateNameIndex ON Template(name);

CREATE TABLE IF NOT EXISTS TemplateField (
    id INTEGER PRIMARY KEY,
    template_id INTEGER REFERENCES Template,
    name TEXT COLLATE NOCASE,
    description TEXT,
    format INTEGER,
    reference_category_id INTEGER REFERENCES Category, -- if set, value will refer to a Document in this category
    default_value BLOB,
    sort_order INTEGER,
    created INTEGER,
    modified INTEGER,
    created_by_user_id INTEGER REFERENCES User,
    modified_by_user_id INTEGER REFERENCES User
);
