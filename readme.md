# Infinity CMS

Infinity is a light web framework and CMS written in PHP. It's just a hobby project for my own use.

![mockups](https://bytebucket.org/MatthewJuhl/infinity-2/raw/fa5c758cceb27f9b64aad4b9680a154c22bec5be/design%20files/layout.sketch/QuickLook/Preview.png)

## Project setup

Use the [project template](https://bitbucket.org/MatthewJuhl/infinity-project-template) to create a new site/app.


## Glossary

* **Field:** basically a key/value pair with some descriptive properties
* **Document:** a set of fields
* **Template:** a set of fields with default values for creating content documents
* **Draft:** an unpublished version of a document
* **DraftField:** a field that belongs to a draft. only created if it differs from
                  the published version.
* **Category:** a structure for organizing documents. can be nested ad infinitum.
* **User:** someone who can login to the site
    * **Developer:** an unrestricted administrative user
    * **Administrator:** a user who can access all CMS features and site settings
    * **Editor:** a user who can access all CMS features
    * **Staff:** a user who can create and edit drafts in the CMS (but not publish)
    * **User:** a user of the site with no CMS access


## Upcoming Features
* flash messages
* user authentication
* basic content management (client)
* basic content document templating (developer)
* basic user management and information (developer)
* developer and admin permissions setup

## Possible Future Features and Improvements
* improve framework code
    * better separation of concerns and isolation of tasks and variables
    * better abstraction and wrapping of native functionality for flexibility
* class autoloading
* content draft authoring and publishing
* environment-aware tools for deploying
* form helpers
* squarespace style content editor


## Helpful information for frontend build

* [shrinkwrap dependencies](http://www.andismith.com/blog/2014/02/shrinkwrap-your-dependencies/)
* (http://www.browsersync.io/docs/gulp/)