<?
// TODO: this file should be renamed/removed?

# you'll want to change this in production:
ini_set('display_errors', 'On');
error_reporting(E_ALL);

# set your timezone. all dates/times will be relative to this.
# the list is located at [http://php.net/manual/en/timezones.php]
date_default_timezone_set('America/Chicago');

# start by including Infinity's bootstrap file.
require_once 'infinity/bootstrap.php';

# initialize infinity
$infinity = new \Infinity\Infinity;

# grab the path to your website's directory
$local_path = dirname(__FILE__);

# set your configuration options here:
$infinity->setConfig([
    # easy stuff
    'site_name' => 'My Infinitely Awesome Website',
    'developer_email' => 'desk@matthewjuhl.com',

    # This simply tells Infinity where we are. You don't need to change this.
    'root_directory' => $local_path,

    # This tells Infinity where the code for your site lives. You don't need to change this.
    'website_directory' => $local_path . DIRECTORY_SEPARATOR . 'website',

    # Set the location of your data directory. This is where Infinity will store
    # your content database, encryption keys, and so forth. Optimally this
    # directory would be located outside of your web server root somwhere not
    # publicly accessible. You should change this (but you don't have to).
    'data_directory' => $local_path . DIRECTORY_SEPARATOR . 'data',

    # Infinity creates a sqlite database file for you in the data directory specified
    # above. Change the filename here (if you want).
    'db_filename' => 'infinity_data.db',

    'auto_load_content' => FALSE,

    # If you want to turn on Infinity's internal logging for debug purposes, set
    # a file name here to write to. Debug messages will get written to this file
    # during each request. If you don't need this functionality, comment or delete
    # this line:
    'infinity_log_file' => $local_path . DIRECTORY_SEPARATOR . 'data/infinity.log'
]);

$infinity->view->setLayout('main-layout');
$infinity->defineRoutes([
    ['*', '/about', 'index', 'about']
]);
# tell Infinity you're done with configuration to begin handling the request
$infinity->launch();
